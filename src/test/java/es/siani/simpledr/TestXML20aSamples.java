package es.siani.simpledr;

import java.io.IOException;
import java.net.URL;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.*;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.xml.sax.SAXException;

@RunWith(JUnit4.class)
public class TestXML20aSamples extends TestCase {
	
	protected Validator validator;
	
	/**
	 * Setup the member variables.
	 */
	@Before
	public void setUp() throws Exception {
		
		SchemaFactory schemaFactory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");		
		
		// Load the XSD of the profile 2.0a
		URL schemaURL = this.getClass().getResource("/schemas/20a/oadr_20a.xsd");
		this.validator = schemaFactory.newSchema(schemaURL).newValidator();
		
		return;
	}
	

	/**
	 * Test if the sample "basic-a.xml" is compliant with the XSD schemas of the profile A.
	 */
	@Test
	public void testMinimal() {

		Source xmlSource = new StreamSource(this.getClass().getResourceAsStream("/samples/20a/minimal.xml"));
		
		try {
			this.validator.validate(xmlSource);
		} catch (SAXException ex) {
			fail("The XML is not valid.\n" + ex.getMessage());
		} catch (IOException ex) {
			fail("The XML source file was not found.");
		}
		
		return;
	}
	
	
	/**
	 * Test if the sample "simple.xml" is compliant with the XSD schema of the profile A.
	 */
	@Test
	public void testSimple() {

		Source xmlSource = new StreamSource(this.getClass().getResourceAsStream("/samples/20a/simple.xml"));
		
		try {
			this.validator.validate(xmlSource);
		} catch (SAXException ex) {
			fail("The XML is not valid.\n" + ex.getMessage());
		} catch (IOException ex) {
			fail("The XML source file was not found.");
		}
		
		return;
	}
}
