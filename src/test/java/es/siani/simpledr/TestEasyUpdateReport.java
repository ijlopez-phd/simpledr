package es.siani.simpledr;

import java.io.StringReader;
import java.io.StringWriter;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import org.junit.Test;
import es.siani.simpledr.xml.AsdrPrefixMapper;
import junit.framework.TestCase;

public class TestEasyUpdateReport extends TestCase {
	
//	private static final String REPORT_NAME = "x-as-load-demand";
//	private static final String REPORT_SPECIFIER = "rep-as-01";
	private static final String NODE_CODE = "VTN00";
	private static final HashMap<String, Float> payloads = new HashMap<String, Float>(6);
	
	static {
		payloads.put("level0", 30f);
		payloads.put("level1", 20f);
		payloads.put("level2", 10f);
		payloads.put("level3", 0f);
		payloads.put("hard", 15f);
		payloads.put("easy", 10f);		
	}
	
	private Validator validator;
	private Marshaller marshaller;
	
	
	/**
	 * Setup global resources before each unit test.
	 */
	@Override
	protected void setUp() throws Exception {
		
		SchemaFactory schemaFactory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
		
		// Load the XSD of the profile 2.0b
		URL schemaURL = this.getClass().getResource("/schemas/20b/oadr_20b.xsd");
		this.validator = schemaFactory.newSchema(schemaURL).newValidator();
		
		JAXBContext jaxbContext = JAXBContext.newInstance(OadrModel.NAMESPACE);
		this.marshaller = jaxbContext.createMarshaller();
		this.marshaller.setProperty("com.sun.xml.bind.namespacePrefixMapper", new AsdrPrefixMapper());
		
		return;
	}
	
	
	/**
	 * Test the constructor of "EasyReport" that creates a new
	 * instance from scratch.
	 */
	@Test
	public void testContructorFromScratch() {
		
		EasyCreateReport createReport = new EasyCreateReport(
				new StreamSource(
						this.getClass().getResourceAsStream("/samples/20b/createReport-simple.xml")));
		
		EasyUpdateReport easyUpdate = new EasyUpdateReport(NODE_CODE, createReport, payloads);
		
		// Serialize the OadrUpdateReport and validate it in order to confirm that the
		// information from which it has been created is compliant with the specifications.
		StringWriter ostream = new StringWriter();
		try {
			this.marshaller.marshal(easyUpdate.getReport(), ostream);
		} catch (JAXBException ex) {
			fail("Failed to marshall the OadrUpdateReport object.");
		}
		
		assertNotNull(ostream.toString());
		assertTrue(ostream.toString().length() > 0);
		
		try {
			this.validator.validate(new StreamSource(new StringReader(ostream.toString())));
		} catch (Exception ex) {
			fail("Falied to validate the XML corresponding to the OadrUpdateReport.");
		}
		
		return;
	}
	
	
	/**
	 * Test the constructor that creates an EasyrUpdateReport from the XML corresponding
	 * to an OadrUpdateReport.
	 */
	@Test
	public void testConstructorSource() {
		
		Source xmlSource =
				new StreamSource(this.getClass().getResourceAsStream("/samples/20b/updateReport-simple.xml"));
		EasyUpdateReport easy = new EasyUpdateReport(xmlSource);
		
		assertNotNull(easy.getReport());
	
		return;
	}
	
	
	/**
	 * Test getting values from a OadrReport.
	 */
	@Test
	public void testGetValues() {
		
		Source xmlSource =
				new StreamSource(this.getClass().getResourceAsStream("/samples/20b/updateReport-simple.xml"));
		EasyUpdateReport easy = new EasyUpdateReport(xmlSource);
		
		Map<String, Float> values = easy.getValues();
		assertEquals(30f, values.get("level0"), 0.001);
		assertEquals(20f, values.get("level1"), 0.001);
		assertEquals(10f, values.get("level2"), 0.001);
		assertEquals(0f, values.get("level3"), 0.001);
		assertEquals(15f, values.get("hard"), 0.001);
		assertEquals(10f, values.get("easy"), 0.001);
		
		return;
	}
	
	
	/**
	 * Test the conversion to string.
	 */
	@Test
	public void testToString() {
		
		EasyCreateReport createReport = new EasyCreateReport(
				new StreamSource(
						this.getClass().getResourceAsStream("/samples/20b/createReport-simple.xml")));
		
		EasyUpdateReport easy = new EasyUpdateReport(NODE_CODE, createReport, payloads);
		
		try {
			this.validator.validate(new StreamSource(new StringReader(easy.toString())));
		} catch (Exception ex) {
			fail("Failed to validate the XML returned by the 'toString' method of EasyUpdateReport.");
		}
		
		return;
	}

}
