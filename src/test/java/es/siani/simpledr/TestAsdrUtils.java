package es.siani.simpledr;

import java.io.InputStream;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import es.siani.simpledr.AsdrException;
import es.siani.simpledr.AsdrUtils;
import es.siani.simpledr.model.AsdrEvent;
import es.siani.simpledr.model.AsdrInterval;
import es.siani.simpledr.model.AsdrProgram;
import es.siani.simpledr.model.openadr.OadrDistributeEvent.OadrEvent;
import es.siani.simpledr.model.openadr.ei.EiEvent;
import es.siani.simpledr.model.openadr.ei.Interval;
import es.siani.simpledr.model.openadr.ei.PayloadFloatType;
import es.siani.simpledr.model.openadr.ei.SignalPayload;
import static org.junit.Assert.*;
import static es.siani.simpledr.OadrUtils.*;

@RunWith(JUnit4.class)
public class TestAsdrUtils {
	
	protected DatatypeFactory typeFactory;
	private static final String URI_MARKET_CONTEXT = "http://www.siani.es";
	
	
	/**
	 * Setup the member variables.
	 */
	@Before
	public void setUp() throws Exception {
		this.typeFactory = DatatypeFactory.newInstance();
		return;
	}
	
	
	/**
	 * Check the creation of the DR program defined in the sample "p20a_simple_01.xml".
	 */
	@Test
	public void testCreateProgramSimple01() {
		InputStream istream = this.getClass().getResourceAsStream("/samples/program/p20a_simple_01.xml");
		AsdrProgram program;
		try {
			program = AsdrUtils.createProgram(istream);
		} catch (AsdrException e) {
			fail("Error ocurred during parsing and validating the XML stream.");
			return;
		}
		
		assertEquals(program.getName(), "simpleProgram01");
		assertEquals(program.getEvents().size(), 2);
		
		AsdrEvent event = program.getEvents().get(1);
		
		assertEquals(event.getSignalType().value(), "level");
		assertEquals(
				event.getStart().getTime(),
				typeFactory.newXMLGregorianCalendar("2013-01-01T00:01:00Z").normalize().toGregorianCalendar().getTimeInMillis());
		assertEquals(event.getPriority(), 1);
		assertEquals(event.getDuration(), 7800);
		assertEquals(event.getIntervals().size(), 3);
		
		AsdrInterval interval = event.getIntervals().get(2);
		
		assertEquals(interval.getDuration(), 600);
		assertEquals(interval.getPayload(), 3f, 0.001);		
		
		return;
	}
	
	
	/**
	 * Check the creation of the program defined in the sample "p20a_minimal_01.xml".
	 */
	@Test
	public void testCreateProgramMinimal01() {
		InputStream istream = this.getClass().getResourceAsStream("/samples/program/p20a_minimal_01.xml");
		AsdrProgram program;
		try {
			program = AsdrUtils.createProgram(istream);
		} catch (AsdrException e) {
			fail("Error occurred during parsing and validating the XML stream.");
			return;
		}
		
		assertEquals(program.getName(), "minimalProgram01");
		assertEquals(program.getEvents().size(), 1);
		
		AsdrEvent event = program.getEvents().get(0);
		
		assertEquals(event.getSignalType().value(), "setpoint");
		assertEquals(
					event.getStart().getTime(), 
					typeFactory.newXMLGregorianCalendar("2013-01-01T00:00:00Z").normalize().toGregorianCalendar().getTimeInMillis());
		assertEquals(event.getPriority(), AsdrEvent.NO_PRIORITY);
		assertEquals(event.getDuration(), 120);
		assertEquals(event.getIntervals().size(), 1);
		
		AsdrInterval interval = event.getIntervals().get(0);
		
		assertEquals(interval.getDuration(), 120);
		assertEquals(interval.getPayload(), 1f, 0.001);
		
		return;
	}
	
	
	/**
	 * Test the conversion from OadrEvent to AsdrEvent.
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void testToAsdrEvent() {
		Source xmlSource = new StreamSource(this.getClass().getResourceAsStream(("/samples/20b/distributeEvent-manyEvents.xml")));
		EasyDistributeEvent easy = new EasyDistributeEvent(URI_MARKET_CONTEXT, xmlSource);
		
		for (OadrEvent oadrEvent : easy.getDistributeEvent().getOadrEvents()) {
			AsdrEvent asdrEvent = AsdrUtils.toAsdrEvent(oadrEvent);
			final EiEvent eiEvent = oadrEvent.getEiEvent();
			assertEquals(eiEvent.getEventDescriptor().getEventID(), asdrEvent.getEventId());
			assertEquals(
					eiEvent.getEiEventSignals().getEiEventSignals().get(0).getSignalType().value(),
					asdrEvent.getSignalType().value());
			assertEquals(
					eiEvent.getEiActivePeriod().getProperties().getDtstart().getDateTime().getValue()
							.toGregorianCalendar().getTime().getTime(),
					asdrEvent.getStart().getTime());
			assertEquals(
					xmlDurationToMilliseconds(
							eiEvent.getEiActivePeriod().getProperties().getDuration().getDuration().getValue()) / 1000,
					asdrEvent.getDuration());
			assertEquals(
					OadrUtils.xmlDurationToMilliseconds(
							eiEvent.getEiActivePeriod().getProperties().getXEiNotification().getDuration().getValue()) / 1000,
					asdrEvent.getNotificationDuration());
			assertEquals(
					(long)eiEvent.getEventDescriptor().getPriority().longValue(),
					asdrEvent.getPriority());
			
			List<Interval> oadrIntervals = eiEvent.getEiEventSignals().getEiEventSignals().get(0).getIntervals().getIntervals();
			List<AsdrInterval> asdrIntervals = asdrEvent.getIntervals();
			assertEquals(oadrIntervals.size(), asdrIntervals.size());
			
			Iterator<AsdrInterval> itAsdr = asdrIntervals.iterator();
			Iterator<Interval> itOadr = oadrIntervals.iterator();
			while (itAsdr.hasNext()) {
				AsdrInterval asdrInterval = itAsdr.next();
				Interval oadrInterval = itOadr.next();
				assertEquals(
						asdrInterval.getPayload(),
						((JAXBElement<PayloadFloatType>)
								((JAXBElement<SignalPayload>)oadrInterval.getStreamPayloadBases().get(0))
								.getValue().getPayloadBase()).getValue().getValue(),
						0.01);
				assertEquals(
						asdrInterval.duration,
						xmlDurationToMilliseconds(oadrInterval.getDuration().getDuration().getValue()) / 1000);
			}
					
		}
		
		return;
	}
}
