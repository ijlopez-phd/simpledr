package es.siani.simpledr;

import static es.siani.simpledr.OadrUtils.toXMLGregorianCalendar;
import static es.siani.simpledr.OadrUtils.xmlDurationToMilliseconds;

import java.util.GregorianCalendar;

import javax.xml.datatype.XMLGregorianCalendar;

import org.junit.Test;

import junit.framework.TestCase;

public class TestOadrUtils extends TestCase {
	
	/**
	 * Test the conversion of a XML duration string into milliseconds.
	 */
	@Test
	public void testXmlDurationToMilliseconds() {
		
		String xmlDuration = "PT300S";
		long millis = xmlDurationToMilliseconds(xmlDuration);
		assertEquals(300 * 1000, millis);
		
		xmlDuration = "PT300M";
		millis = xmlDurationToMilliseconds(xmlDuration);
		assertEquals(300 * 1000 * 60, millis);
		
		xmlDuration = "PT0S";
		millis = xmlDurationToMilliseconds(xmlDuration);
		assertEquals(0, millis);
		
		return;
	}
	
	
	/**
	 * Tests the conversion between XML and Java dates.
	 */
	@Test
	public void testToXMLGregorianCalendar() {
		
		GregorianCalendar gCalendar = new GregorianCalendar(2013, 0, 1);
		XMLGregorianCalendar xmlCalendar = toXMLGregorianCalendar(gCalendar.getTime());
		
		assertEquals(xmlCalendar.toGregorianCalendar().getTimeInMillis(), gCalendar.getTimeInMillis());
		assertEquals("2013-01-01T00:00:00.000Z", xmlCalendar.toXMLFormat());
		
		gCalendar = new GregorianCalendar(2013, 11, 31, 23, 59, 59);
		xmlCalendar = toXMLGregorianCalendar(gCalendar.getTime());
		
		assertEquals(xmlCalendar.toGregorianCalendar().getTimeInMillis(), gCalendar.getTimeInMillis());
		assertEquals("2013-12-31T23:59:59.000Z", xmlCalendar.toXMLFormat());
		
		return;
	}	

}
