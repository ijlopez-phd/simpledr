package es.siani.simpledr;

import java.io.StringReader;
import java.io.StringWriter;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.*;
import es.siani.simpledr.xml.AsdrPrefixMapper;

@RunWith(JUnit4.class)
public class TestEasyCreateReport {
	
	private static final String NODE_CODE = "VTN00";
	//private static final String REPORT_NAME = "x-as-load-demand";
	private static final long GRANULARITY = 60;
	private static final long DURATION = 3600;
	private static final Date START_TIME = new Date();
	private static final String REPORT_SPECIFIER = "rep-as-01";
	private static final String[] RESOURCES_IDS = new String[] {
		"level0", "level1", "level2", "level3", "hard", "easy"
	};
	private static final TimeZone UTC_TZ = TimeZone.getTimeZone("UTC");
	private Validator validator;
	private Marshaller marshaller;
	
	
	/**
	 * Setup the resources before each unit test.
	 */
	@Before
	public void setUp() throws Exception {
		
		SchemaFactory schemaFactory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
		
		// Load the XSD of the profile 2.0b
		URL schemaURL = this.getClass().getResource("/schemas/20b/oadr_20b.xsd");
		this.validator = schemaFactory.newSchema(schemaURL).newValidator();
		
		JAXBContext jaxbContext = JAXBContext.newInstance(OadrModel.NAMESPACE);
		this.marshaller = jaxbContext.createMarshaller();
		this.marshaller.setProperty("com.sun.xml.bind.namespacePrefixMapper", new AsdrPrefixMapper());
		
		return;		
		
	}
	
	
	/**
	 * Test the constructor that instances a new EasyCreateReport from
	 * input arguments that specify the SpecifierId and the interval.
	 */
	@Test
	public void testConstructor() {
		
		EasyCreateReport easy = new EasyCreateReport(
				NODE_CODE, REPORT_SPECIFIER, START_TIME, DURATION, GRANULARITY, RESOURCES_IDS);
		
		// Validate that the instance created in this way is compliant with the XSD Schemas.
		StringWriter ostream = new StringWriter();
		try {
			Marshaller marshaller = JAXBContext.newInstance(OadrModel.NAMESPACE).createMarshaller();
			marshaller.setProperty("com.sun.xml.bind.namespacePrefixMapper", new AsdrPrefixMapper());
			marshaller.marshal(easy.getCreateReport(), ostream);
		} catch (JAXBException ex) {
			fail("Failed to convert the OadrCreateReport object.");
		}
		
		try {
			this.validator.validate(new StreamSource(new StringReader(ostream.toString())));
		} catch (Exception ex) {
			fail("Failed to validate the XML corresponding to the OadrCreateReport.");
		}
		
		return;
	}
	
	
	/**
	 * Test the constructor that builds a CreateReport from a XML source.
	 */
	@Test
	public void testConstructorSource() {
		Source xmlSource =
				new StreamSource(this.getClass().getResourceAsStream("/samples/20b/createReport-simple.xml"));
		
		Calendar cal = Calendar.getInstance(UTC_TZ); 
		cal.set(2013, Calendar.NOVEMBER, 4, 16, 25, 26); // Change these values!!! whenever the sample XML is changed.
		cal.set(Calendar.MILLISECOND, 533);
		
		EasyCreateReport easy = new EasyCreateReport(xmlSource);
		assertEquals(REPORT_SPECIFIER, easy.getSpecifierId());
		assertTrue(Math.abs(cal.getTimeInMillis() - easy.getStartTime().getTime()) <= 1);
		assertEquals(DURATION, easy.getDuration());
		assertEquals(GRANULARITY, easy.getGranularity());
		
		return;
	}
	
	
	/**
	 * Test that the 'getDuration' method retrieves the correct value from a CreateReport object.
	 */
	@Test
	public void testGeDuration() {
		EasyCreateReport easy = new EasyCreateReport(
				NODE_CODE, REPORT_SPECIFIER, START_TIME, DURATION, GRANULARITY, RESOURCES_IDS);
		assertEquals(DURATION, easy.getDuration());
		return;
	}
	
	
	/**
	 * Test that the 'getStartTime' method retrieves the correct value from a CreateReport object.
	 */
	@Test
	public void testGetStartTime() {
		EasyCreateReport easy = new EasyCreateReport(
				NODE_CODE, REPORT_SPECIFIER, START_TIME, DURATION, GRANULARITY, RESOURCES_IDS);
		assertTrue(Math.abs(START_TIME.getTime() - easy.getStartTime().getTime()) <= 1);
		return;
	}
	
	
	/**
	 * Test that the 'getSpecifierId' method retrieves the specifierId from a CreateReport object.
	 */
	@Test
	public void testGetSpecifierId() {
		EasyCreateReport easy = new EasyCreateReport(
				NODE_CODE, REPORT_SPECIFIER, START_TIME, DURATION, GRANULARITY, RESOURCES_IDS);
		assertEquals(REPORT_SPECIFIER, easy.getSpecifierId());
		return;
	}
	
	
	/**
	 * Test the 'getGranularity' method.
	 */
	@Test
	public void testGetGranularity() {
		EasyCreateReport easy = new EasyCreateReport(
				NODE_CODE, REPORT_SPECIFIER, START_TIME, DURATION, GRANULARITY, RESOURCES_IDS);
		assertEquals(GRANULARITY, easy.getGranularity());
		return;
	}
	
	
	/**
	 * Test the conversion of a CreateReport object into a XML stream.
	 */
	@Test
	public void testToString() {
		
		EasyCreateReport easy = new EasyCreateReport(
				NODE_CODE, REPORT_SPECIFIER, START_TIME, DURATION, GRANULARITY, RESOURCES_IDS);
		
		// Convert the object into XML, and validate the content in order to confirm that
		// it is valid.
		try {
			this.validator.validate(new StreamSource(new StringReader(easy.toString())));
		} catch (Exception ex) {
			fail("Failed to validate the XML corresponding to the 'toString' method.");
		}
		
		return;
	}
}
