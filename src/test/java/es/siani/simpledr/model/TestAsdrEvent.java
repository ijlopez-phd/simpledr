package es.siani.simpledr.model;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class TestAsdrEvent {
	
	private final static TimeZone UTC_TZ = TimeZone.getTimeZone("UTC");
	
	
	/**
	 * Test getting an array with the intervals' payloads.
	 */
	@Test
	public void TestGetIntervalsPayload() {
		
		float[] sourcePayloads = new float[] {10.1f, 20.2f, 30.3f, 40.4f, 50.5f};
		
		List<AsdrInterval> intervals = new ArrayList<AsdrInterval>();
		for (float pyld : sourcePayloads) {
			intervals.add(new AsdrInterval(300l, pyld));	
		}
		
		AsdrEvent event = new AsdrEvent(AsdrSignalTypeEnum.DELTA, new Date(), 1800l, 0l, intervals);
		float[] gottenPayloads = event.getIntervalsPayload();
		
		assertEquals(sourcePayloads.length, gottenPayloads.length);
		
		final int N_ELEMS = sourcePayloads.length;
		for (int n = 0; n < N_ELEMS; n++) {
			assertEquals(sourcePayloads[n], gottenPayloads[n], 0.01);
		}
		
		return;
	}
	
	
	/**
	 * Test getting an array with the intervals' duration.
	 */
	@Test
	public void TestGetIntervalsDuration() {
		long[] sourceDurations = new long[] {10l, 20l, 30l, 40l, 50l};
		List<AsdrInterval> intervals = new ArrayList<AsdrInterval>();
		for (long dur : sourceDurations) {
			intervals.add(new AsdrInterval(dur, 1f));
		}
		
		AsdrEvent event = new AsdrEvent(AsdrSignalTypeEnum.DELTA, new Date(), 1800l, 0l, intervals);
		long[] gottenDurations = event.getIntervalsDuration();
		
		assertEquals(sourceDurations.length, gottenDurations.length);
		final int N_ELEMS = sourceDurations.length;
		for (int n = 0; n < N_ELEMS; n++) {
			assertEquals(sourceDurations[n], gottenDurations[n]);
		}
		
		return;
	}
	
	
	/**
	 * Test calculating the end date of an event.
	 */
	@Test
	public void TestGetEndDate() {
		long[] sourceDurations = new long[] {3600l, 3600l};
		List<AsdrInterval> intervals = new ArrayList<AsdrInterval>();
		for (long dur : sourceDurations) {
			intervals.add(new AsdrInterval(dur, 1f));
		}
		
		Calendar cal = Calendar.getInstance(UTC_TZ);
		cal.set(2013, 0, 1, 0, 0, 0);
		AsdrEvent event = new AsdrEvent(AsdrSignalTypeEnum.LEVEL, cal.getTime(), 1800l, 0l, intervals);
		
		cal.set(2013, 0, 1, 2, 0, 0);
		assertEquals(cal.getTime(), event.getEndTime());
		
		return;
	}
	

}
