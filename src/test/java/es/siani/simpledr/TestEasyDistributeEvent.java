package es.siani.simpledr;


import java.io.StringReader;
import java.io.StringWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import es.siani.simpledr.model.openadr.ei.EventDescriptor;
import es.siani.simpledr.model.openadr.ei.EventStatusEnumeratedType;
import es.siani.simpledr.model.openadr.ei.Interval;
import es.siani.simpledr.model.openadr.ei.SignalPayload;
import es.siani.simpledr.model.openadr.ei.PayloadFloatType;
import es.siani.simpledr.model.openadr.xcal.Properties;
import es.siani.simpledr.model.AsdrEvent;
import es.siani.simpledr.model.AsdrInterval;
import es.siani.simpledr.model.AsdrSignalPayloadEnum;
import es.siani.simpledr.model.AsdrSignalTypeEnum;
import es.siani.simpledr.model.openadr.OadrDistributeEvent;
import es.siani.simpledr.model.openadr.OadrDistributeEvent.OadrEvent;
import es.siani.simpledr.xml.AsdrPrefixMapper;
import junit.framework.TestCase;

@RunWith(JUnit4.class)
public class TestEasyDistributeEvent extends TestCase {
	
	private static final String NODE_CODE = "VTN00";
	private static final String URI_MARKET_CONTEXT = "http://www.siani.es";
	
	private Validator validator;
	private Marshaller marshaller;
	
	
	
	/**
	 * Setup the resources before each unit test.
	 */
	@Before
	public void setUp() throws Exception {
		
		SchemaFactory schemaFactory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
		
		// Load the XSD of the profile 2.0b
		URL schemaURL = this.getClass().getResource("/schemas/20b/oadr_20b.xsd");
		this.validator = schemaFactory.newSchema(schemaURL).newValidator();
		
		JAXBContext jaxbContext = JAXBContext.newInstance(OadrModel.NAMESPACE);
		this.marshaller = jaxbContext.createMarshaller();
		this.marshaller.setProperty("com.sun.xml.bind.namespacePrefixMapper", new AsdrPrefixMapper());
		
		return;
	}
	
	
	/**
	 * Test the constructor of EasyDistributeEvent that creates
	 * an OadrDistributeEvent from a Source.
	 */
	@Test
	public void testConstructorSource() {
		Source xmlSource =
				new StreamSource(this.getClass().getResourceAsStream(("/samples/20b/distributeEvent-simple.xml")));
		
		EasyDistributeEvent easy = new EasyDistributeEvent(URI_MARKET_CONTEXT, xmlSource);
		OadrDistributeEvent oadrMessage = easy.getDistributeEvent(); 
		
		assertEquals("1", oadrMessage.getVtnID());
		assertEquals(1, oadrMessage.getOadrEvents().size());
		
		return;
	}
	
	
	/**
	 * Test the constructor of EasyDistributeEvent that creates an
	 * empty OadrDistributeEvent.
	 */
	@Test
	public void testCronstructorNodeCode() {
		
		OadrDistributeEvent oadrMessage = 
				new EasyDistributeEvent(URI_MARKET_CONTEXT, NODE_CODE).getDistributeEvent();
		
		// Test the fields' values of the OadrDistributeEvent.
		assertEquals(NODE_CODE, oadrMessage.getVtnID());
		assertEquals(oadrMessage.getRequestID(), OadrFactory.getLastRequestId(NODE_CODE));
		assertEquals(0, oadrMessage.getOadrEvents().size());
		
		
		// Serialize the message and validate it in order to confirm that this
		// minimum set of information is compliance with the specifcations.
		StringWriter ostream = new StringWriter();
		try {
			this.marshaller.marshal(oadrMessage, ostream);
		} catch (JAXBException ex) {
			fail("The marshalling of the oadrDistributeEvent object failed.");
		}		
		
		assertNotNull(ostream.toString());
		assertTrue(ostream.toString().length() > 0);
		
		try {
			this.validator.validate(new StreamSource(new StringReader(ostream.toString())));
		} catch (Exception ex) {
			fail("Falied to validate the XML corresponding to the oadrDistributeEvent.");
		}
		
		return;
	}
	
	
	/**
	 * Tests the addition of many events.
	 */
	@Test
	public void testAddManyEvents() {
		
		EasyDistributeEvent easy = new EasyDistributeEvent(URI_MARKET_CONTEXT, NODE_CODE);
		
		ArrayList<AsdrInterval> intervals = new ArrayList<AsdrInterval>();
		intervals.add(new AsdrInterval(3600, AsdrSignalPayloadEnum.MODERATE.value()));
		AsdrEvent event = new AsdrEvent(
				AsdrSignalTypeEnum.LEVEL,
				new GregorianCalendar(2013, 11, 31, 23, 59, 59).getTime(),
				60*5,
				AsdrEvent.NO_PRIORITY,
				intervals);
		
		
		final long modelTime = new GregorianCalendar(2013, 11, 31, 23, 59, 59).getTimeInMillis();
		easy.addEvent(event, modelTime);
		easy.addEvent(event, modelTime);
		easy.addEvent(event, modelTime);
		OadrDistributeEvent oadrMessage = easy.getDistributeEvent();
		
		assertEquals(3, oadrMessage.getOadrEvents().size());
	
		return;
	}
	

	/**
	 * Tests the addition of new events to oadrDistributeEvent.
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void testAddEvent() {
		
		// Create the OadrDistributeEvent message.
		EasyDistributeEvent easy = new EasyDistributeEvent(URI_MARKET_CONTEXT, NODE_CODE);
		OadrDistributeEvent oadrMessage = easy.getDistributeEvent();
		assertEquals(0, oadrMessage.getOadrEvents().size());
		
		// Create the event to be added
		final GregorianCalendar startCalendar = new GregorianCalendar(2013, 11, 31, 23, 59, 59);
		final long intervalDuration = 3600;
		final float intervalValue = AsdrSignalPayloadEnum.MODERATE.value();
		final long notifDuration = 5*60;
		ArrayList<AsdrInterval> intervals = new ArrayList<AsdrInterval>();
		intervals.add(new AsdrInterval(intervalDuration, intervalValue));
		AsdrEvent event = new AsdrEvent(
				AsdrSignalTypeEnum.LEVEL,
				startCalendar.getTime(),
				notifDuration, // notification (5 minutes before).
				AsdrEvent.NO_PRIORITY,
				intervals);
		
		// Add the new event
		final long modelTime = new GregorianCalendar(2013, 11, 31, 23, 59, 59).getTimeInMillis();		
		easy.addEvent(event, modelTime);
		oadrMessage = easy.getDistributeEvent();
		assertEquals(1, oadrMessage.getOadrEvents().size());
		
		// Check the vaules of the event added to the message.
		OadrEvent oadrEvent = oadrMessage.getOadrEvents().get(0);
		assertEquals(
				event.isResponseRequired()? "always" : "never",
				oadrEvent.getOadrResponseRequired().value());
		
		assertEquals(
				event.getPriority(),
				oadrEvent.getEiEvent().getEventDescriptor().getPriority().longValue());
		
		assertEquals(
				1,
				oadrEvent.getEiEvent().getEiEventSignals().getEiEventSignals().get(0)
					.getIntervals().getIntervals().size());
		
		
		Properties props = oadrEvent.getEiEvent().getEiActivePeriod().getProperties();
		assertEquals(
				startCalendar.getTimeInMillis(),
				props.getDtstart().getDateTime().getValue().normalize().toGregorianCalendar().getTimeInMillis());
		
		assertEquals(
				"PT" + event.getNotificationDuration() + "S",
				oadrEvent.getEiEvent().getEiActivePeriod().getProperties().getXEiNotification().getDuration().getValue());
		
		Interval oadrInterval = 
				oadrEvent.getEiEvent().getEiEventSignals().getEiEventSignals().get(0)
					.getIntervals().getIntervals().get(0);
		assertEquals("PT" + intervalDuration + "S", oadrInterval.getDuration().getDuration().getValue());
		assertEquals(((JAXBElement<PayloadFloatType>)
				(((JAXBElement<SignalPayload>)oadrInterval.getStreamPayloadBases().get(0)).getValue().getPayloadBase())).getValue().getValue(),
				intervalValue,
				0);
		
		return;
	}
	
	/**
	 * Test the status assignation to the events.
	 */
	@Test
	public void testGetEventStatus() {
		ArrayList<AsdrInterval> intervals = new ArrayList<AsdrInterval>();
		intervals.add(new AsdrInterval(3600, AsdrSignalPayloadEnum.MODERATE.value()));
		AsdrEvent event = new AsdrEvent(
				AsdrSignalTypeEnum.LEVEL,
				new GregorianCalendar(2013, 0, 1, 1, 0, 0).getTime(), // 2013-01-01T01:00:00Z.
				60*5, // 5 minutes
				AsdrEvent.NO_PRIORITY,
				intervals);
		
		long modelTime = new GregorianCalendar(2013, 0, 1, 0, 0, 0).getTimeInMillis();
		assertEquals(EventStatusEnumeratedType.NONE, EasyDistributeEvent.getEventStatus(event, modelTime));
		
		modelTime = new GregorianCalendar(2013, 0, 1, 0, 55, 0).getTimeInMillis();
		assertEquals(EventStatusEnumeratedType.FAR, EasyDistributeEvent.getEventStatus(event, modelTime));
		
		modelTime = new GregorianCalendar(2013, 0, 1, 1, 5, 0).getTimeInMillis();
		assertEquals(EventStatusEnumeratedType.ACTIVE, EasyDistributeEvent.getEventStatus(event, modelTime));
		
		modelTime = new GregorianCalendar(2013, 0, 1, 2, 0, 0).getTimeInMillis();
		assertEquals(EventStatusEnumeratedType.COMPLETED, EasyDistributeEvent.getEventStatus(event, modelTime));
		
		return;
	}
	
	
	/**
	 * Test the deletion of the finished and cancelled events.
	 */
	@Test
	public void testRemoveFinishedEvents() {
		Source xmlSource = new StreamSource(this.getClass().getResourceAsStream(("/samples/20b/distributeEvent-manyEvents.xml")));
		EasyDistributeEvent easy = new EasyDistributeEvent(URI_MARKET_CONTEXT, xmlSource);
		
		List<OadrEvent> oadrEvents = easy.getDistributeEvent().getOadrEvents(); 
		oadrEvents.get(0).getEiEvent().getEventDescriptor().setEventStatus(EventStatusEnumeratedType.CANCELLED);
		oadrEvents.get(1)
		.getEiEvent().getEventDescriptor().setEventStatus(EventStatusEnumeratedType.COMPLETED);
		final String eventId = oadrEvents.get(2).getEiEvent().getEventDescriptor().getEventID();
		
		easy.removeFinishedEvents();
		oadrEvents = easy.getDistributeEvent().getOadrEvents();
		
		assertEquals(1, oadrEvents.size());
		assertEquals(eventId, oadrEvents.get(0).getEiEvent().getEventDescriptor().getEventID());
		
		return;
	}
	
	/**
	 * Test the deletion of an event.
	 */
	@Test
	public void testRemoveEvent() {
		
		// Create the OadrDistributeEvent message.
		EasyDistributeEvent easy = new EasyDistributeEvent(URI_MARKET_CONTEXT, NODE_CODE);
		OadrDistributeEvent oadrMessage = easy.getDistributeEvent();
		assertEquals(0, oadrMessage.getOadrEvents().size());
		
		// Create and add a new event.
		final GregorianCalendar startCalendar = new GregorianCalendar(2013, 11, 31, 23, 59, 59);
		final long intervalDuration = 3600;
		final float intervalValue = AsdrSignalPayloadEnum.MODERATE.value();
		final long notifDuration = 5*60;
		ArrayList<AsdrInterval> intervals = new ArrayList<AsdrInterval>();
		intervals.add(new AsdrInterval(intervalDuration, intervalValue));
		AsdrEvent event = new AsdrEvent(
				AsdrSignalTypeEnum.LEVEL,
				startCalendar.getTime(),
				notifDuration, // notification (5 minutes before).
				AsdrEvent.NO_PRIORITY,
				intervals);
		final long modelTime = new GregorianCalendar(2013, 11, 31, 23, 59, 59).getTimeInMillis();		
		String eventId = easy.addEvent(event, modelTime);
		
		oadrMessage = easy.getDistributeEvent();
		assertEquals(1, oadrMessage.getOadrEvents().size());
		
		// Remove the inserted event and test the result.
		easy.removeEvent(eventId);
		assertEquals(0, oadrMessage.getOadrEvents().size());
		
		// Add two events and remove one.
		String evendId_1 = easy.addEvent(event, modelTime);
		String eventId_2 = easy.addEvent(event, modelTime);
		easy.removeEvent(evendId_1);
		
		assertEquals(1, oadrMessage.getOadrEvents().size());
		assertEquals(
				eventId_2,
				oadrMessage.getOadrEvents().get(0).getEiEvent().getEventDescriptor().getEventID());
		
		return;
		
	}
	
	
	/**
	 * Test finding events in other instance that are not registered in the local instance.
	 */
	@Test
	public void testFindNewEvents() {
		Source xmlSource = new StreamSource(this.getClass().getResourceAsStream(("/samples/20b/distributeEvent-manyEvents.xml")));
		EasyDistributeEvent easy = new EasyDistributeEvent(URI_MARKET_CONTEXT, xmlSource);
		xmlSource = new StreamSource(this.getClass().getResourceAsStream(("/samples/20b/distributeEvent-manyEvents.xml")));
		EasyDistributeEvent otherEasy = new EasyDistributeEvent(URI_MARKET_CONTEXT, xmlSource);
		
		// Test with two identical instances of EasyDistributeEvent.
		List<AsdrEvent> foundEvents = easy.findNewEvents(otherEasy);
		assertEquals(0, foundEvents.size());
		
		// Test with the instance 'otherEasy' having one more event.
		String eventId_1 = otherEasy.addEvent(
				new AsdrEvent(AsdrSignalTypeEnum.DELTA, new Date(), 3600, 5.0f),
				new Date().getTime());		
		foundEvents = easy.findNewEvents(otherEasy);
		assertEquals(1, foundEvents.size());
		assertEquals(eventId_1, foundEvents.get(0).getEventId());
		
		
		// Test with the instance 'otherEasy' having two more events.
		String eventId_2 = otherEasy.addEvent(
				new AsdrEvent(AsdrSignalTypeEnum.DELTA, new Date(), 3600, 5.0f),
				new Date().getTime());
		foundEvents = easy.findNewEvents(otherEasy);
		assertEquals(2, foundEvents.size());
		assertEquals(eventId_1, foundEvents.get(0).getEventId());
		assertEquals(eventId_2, foundEvents.get(1).getEventId());
		
		return;
	}
	
	/**
	 * Test the update of the status of the events.
	 */
	@Test
	public void testUpdateEventStatus() {
		
		Source xmlSource = new StreamSource(this.getClass().getResourceAsStream(("/samples/20b/distributeEvent-manyEvents.xml")));
		EasyDistributeEvent easy = new EasyDistributeEvent(URI_MARKET_CONTEXT, xmlSource);
		final OadrDistributeEvent dist = easy.getDistributeEvent();
		
		assertEquals(3, dist.getOadrEvents().size());
		final EventDescriptor descEvent1 = dist.getOadrEvents().get(0).getEiEvent().getEventDescriptor();
		final EventDescriptor descEvent2 = dist.getOadrEvents().get(1).getEiEvent().getEventDescriptor();
		final EventDescriptor descEvent3 = dist.getOadrEvents().get(2).getEiEvent().getEventDescriptor();
		
		GregorianCalendar cal = new GregorianCalendar(2000, 0, 1, 0, 0, 0); // 2000-01-01T00:00:00Z
		easy.updateEventsStatus(cal.getTimeInMillis());
		assertEquals(EventStatusEnumeratedType.NONE, descEvent1.getEventStatus());
		assertEquals(EventStatusEnumeratedType.NONE, descEvent2.getEventStatus());
		assertEquals(EventStatusEnumeratedType.NONE, descEvent3.getEventStatus());
		
		cal.set(2000, 0, 1, 0, 56, 0); // 2000-01-01T00:56:00
		easy.updateEventsStatus(cal.getTimeInMillis());
		assertEquals(EventStatusEnumeratedType.FAR, descEvent1.getEventStatus());
		assertEquals(EventStatusEnumeratedType.NONE, descEvent2.getEventStatus());
		assertEquals(EventStatusEnumeratedType.NONE, descEvent3.getEventStatus());
		
		cal.set(2000, 0, 1, 1, 0, 0); // 2000-01-01T01:00:00
		easy.updateEventsStatus(cal.getTimeInMillis());
		assertEquals(EventStatusEnumeratedType.ACTIVE, descEvent1.getEventStatus());
		assertEquals(EventStatusEnumeratedType.NONE, descEvent2.getEventStatus());
		assertEquals(EventStatusEnumeratedType.NONE, descEvent3.getEventStatus());
		
		cal.set(2000, 0, 1, 1, 25, 0); // 2000-01-01T01:25:00
		easy.updateEventsStatus(cal.getTimeInMillis());
		assertEquals(EventStatusEnumeratedType.ACTIVE, descEvent1.getEventStatus());
		assertEquals(EventStatusEnumeratedType.FAR, descEvent2.getEventStatus());
		assertEquals(EventStatusEnumeratedType.NONE, descEvent3.getEventStatus());
		
		cal.set(2000, 0, 1, 1, 30, 0); // 2000-01-01T01:30:00
		easy.updateEventsStatus(cal.getTimeInMillis());
		assertEquals(EventStatusEnumeratedType.COMPLETED, descEvent1.getEventStatus());
		assertEquals(EventStatusEnumeratedType.ACTIVE, descEvent2.getEventStatus());
		assertEquals(EventStatusEnumeratedType.NONE, descEvent3.getEventStatus());
		
		cal.set(2000, 0, 1, 2, 00, 0); // 2000-01-01T02:00:00
		easy.updateEventsStatus(cal.getTimeInMillis());
		assertEquals(EventStatusEnumeratedType.COMPLETED, descEvent1.getEventStatus());
		assertEquals(EventStatusEnumeratedType.COMPLETED, descEvent2.getEventStatus());
		assertEquals(EventStatusEnumeratedType.NONE, descEvent3.getEventStatus());
		
		cal.set(2000, 0, 1, 2, 11, 0); // 2000-01-01T02:11:00
		easy.updateEventsStatus(cal.getTimeInMillis());
		assertEquals(EventStatusEnumeratedType.COMPLETED, descEvent1.getEventStatus());
		assertEquals(EventStatusEnumeratedType.COMPLETED, descEvent2.getEventStatus());
		assertEquals(EventStatusEnumeratedType.FAR, descEvent3.getEventStatus());
		
		cal.set(2000, 0, 1, 2, 15, 0); // 2000-01-01T02:15:00
		easy.updateEventsStatus(cal.getTimeInMillis());
		assertEquals(EventStatusEnumeratedType.COMPLETED, descEvent1.getEventStatus());
		assertEquals(EventStatusEnumeratedType.COMPLETED, descEvent2.getEventStatus());
		assertEquals(EventStatusEnumeratedType.ACTIVE, descEvent3.getEventStatus());
		
		cal.set(2000, 0, 1, 2, 45, 0); // 2000-01-01T02:45:00
		easy.updateEventsStatus(cal.getTimeInMillis());
		assertEquals(EventStatusEnumeratedType.COMPLETED, descEvent1.getEventStatus());
		assertEquals(EventStatusEnumeratedType.COMPLETED, descEvent2.getEventStatus());
		assertEquals(EventStatusEnumeratedType.COMPLETED, descEvent3.getEventStatus());		
		
		return;
	}	
	
	
	/**
	 * Tests the conversion to string.
	 */
	@Test
	public void testToString() {
		
		EasyDistributeEvent easy = new EasyDistributeEvent(URI_MARKET_CONTEXT, NODE_CODE);
		
		ArrayList<AsdrInterval> intervals = new ArrayList<AsdrInterval>();
		intervals.add(new AsdrInterval(3600, AsdrSignalPayloadEnum.MODERATE.value()));
		AsdrEvent event = new AsdrEvent(
				AsdrSignalTypeEnum.LEVEL,
				new GregorianCalendar(2013, 11, 31, 23, 59, 59).getTime(),
				60*5,
				AsdrEvent.NO_PRIORITY,
				intervals);
		
		final long modelTime = new GregorianCalendar(2013, 11, 31, 23, 59, 59).getTimeInMillis();
		easy.addEvent(event, modelTime);
		
		// Convert it to XML and validate the content in order to confirm
		// that it is valid.
		try {
			this.validator.validate(new StreamSource(new StringReader(easy.toString())));
		} catch (Exception ex) {
			fail("Falied to validate the XML corresponding to the oadrDistributeEvent.");
		}
		
		return;
	}
}
