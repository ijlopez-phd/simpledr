package es.siani.simpledr;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import es.siani.simpledr.OadrFactory;

@RunWith(JUnit4.class)
public class TestOadrFactory {
	
	private static final String NODE_1_CODE = "VTN-1";
	private static final String NODE_2_CODE = "VTN-2";

	
	/**
	 * Setup the member variables.
	 */
	@Before
	public void setUp() throws Exception {
		return;
	}
	

	/**
	 * Check the cache of Events' ids.
	 */
	@Test
	public void testGetNextEventId() {
		OadrFactory.resetEventsIds();
		
		String id = OadrFactory.getNextEventId(NODE_1_CODE);
		assertEquals(0, Integer.parseInt(id));
		
		id = OadrFactory.getNextEventId(NODE_1_CODE);
		assertEquals(1, Integer.parseInt(id));
		
		id = OadrFactory.getNextEventId(NODE_2_CODE);
		assertEquals(0, Integer.parseInt(id));
		
		id = OadrFactory.getNextEventId(NODE_2_CODE);
		assertEquals(1, Integer.parseInt(id));
		
		return;
	}
	
	
	/**
	 * Check the cache of Requests' ids.
	 */
	@Test
	public void testGetNextRequestId() {
		OadrFactory.resetRequestsIds();
		
		String id = OadrFactory.getNextRequestId(NODE_1_CODE);
		assertEquals(0, Integer.parseInt(id));
		
		id = OadrFactory.getNextRequestId(NODE_1_CODE);
		assertEquals(1, Integer.parseInt(id));
		
		id = OadrFactory.getNextRequestId(NODE_2_CODE);
		assertEquals(0, Integer.parseInt(id));
		
		id = OadrFactory.getNextRequestId(NODE_2_CODE);
		assertEquals(1, Integer.parseInt(id));		
		
		return;
	}
	
	
	/**
	 * Check the cache of Reports' ids.
	 */
	@Test
	public void testGetNextReportId() {
		OadrFactory.resetReportsIds();
		
		String id = OadrFactory.getNextReportId(NODE_1_CODE);
		assertEquals(0, Integer.parseInt(id));
		
		id = OadrFactory.getNextReportId(NODE_1_CODE);
		assertEquals(1, Integer.parseInt(id));
		
		id = OadrFactory.getNextReportId(NODE_2_CODE);
		assertEquals(0, Integer.parseInt(id));
		
		id = OadrFactory.getNextReportId(NODE_2_CODE);
		assertEquals(1, Integer.parseInt(id));		
		
		return;		
	}
}
