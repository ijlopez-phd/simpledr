package es.siani.simpledr.model;

public enum AsdrSignalTypeEnum {
	DELTA ("delta"),
	LEVEL ("level"),
	MULTIPLIER ("multiplier"),
	PRICE ("price"),
	PRICE_MULTIPLIER ("priceMultiplier"),
	PRICE_RELATIVE ("priceRelative"),
	PRODUCT ("product"),
	SETPOINT ("setpoint");
	
	
	/** Internal text associated to the item. */
	private String value;
	
	
	/**
	 * Constructor.
	 */
	private AsdrSignalTypeEnum(String text) {
		this.value = text;
		return;
	}
	
	
	/**
	 * Return the text value attached to the item of the enumeration.
	 */
	public String value() {
		return this.value;
	}
}
