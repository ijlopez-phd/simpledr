package es.siani.simpledr.model;

import java.util.List;

public class AsdrTargets {
	
	public List<String> groups = null;
	public List<String> resources = null;
	public List<String> vendors = null;
	public List<String> parties = null;
	
	/**
	 * Constructor.
	 */
	public AsdrTargets() {
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public AsdrTargets(List<String> groups, List<String> resources, List<String> vendors, List<String> parties) {
		this.groups = groups;
		this.resources = resources;
		this.vendors = vendors;
		this.parties = parties;
		return;
	}
}
