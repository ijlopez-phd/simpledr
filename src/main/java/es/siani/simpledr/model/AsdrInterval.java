package es.siani.simpledr.model;


public class AsdrInterval {
	
	/** Duration in seconds of the interval */
	public long duration = 0;
	
	/** Value applied to the signal during the interval */
	public float payload = AsdrSignalPayloadEnum.NORMAL.value();
	
	
	/**
	 * Constructor.
	 */
	public AsdrInterval() {
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public AsdrInterval(long duration, float payload) {
		
		if (duration < 0) {
			throw new IllegalArgumentException("The Duration must be equal or greater than zero.");
		}
		
		this.duration = duration;
		this.payload = payload;
		return;
	}
	
	
	/**
	 * Get the Duration.
	 */
	public long getDuration() {
		return this.duration;
	}
	
	
	/**
	 * Get the Payload.
	 */
	public float getPayload() {
		return this.payload;
	}
}
