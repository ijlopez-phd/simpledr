package es.siani.simpledr.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class AsdrProgram {
	
	/** Default name for the DR programs */
	private static final String NO_NAME = "Generic program";
	
	/** Name of the DR program. */
	protected String name;
	
	/** List of signals of the DR program. */
	List<AsdrEvent> events = new ArrayList<AsdrEvent>();
	
	
	/**
	 * Constructor.
	 */
	public AsdrProgram() {
		this.name = NO_NAME;
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public AsdrProgram(String name) {
		this.name = name;
		return;
	}
	
	
	/**
	 * Creates an empty DR Program.
	 */
	public static AsdrProgram createEmptyProgram() {
		return new AsdrProgram();
	}
	
	
	/**
	 * Set the Name (of the DR program).
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	
	/**
	 * Get the Name (of the DR program).
	 */
	public String getName() {
		return this.name;
	}
	
	
	/**
	 * Add an event to the program.
	 */
	public void addEvent(AsdrEvent event) {
		this.events.add(event);
		return;
	}
	
	
	/**
	 * Add a set of events to the program.
	 */
	public void addEvents(Collection<AsdrEvent> events) {
		this.events.addAll(events);
		return;
	}
	
	
	/**
	 * Get the list of Events.
	 */
	public List<AsdrEvent> getEvents() {
		return this.events;
	}
}
