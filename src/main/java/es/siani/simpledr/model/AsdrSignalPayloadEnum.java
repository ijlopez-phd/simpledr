package es.siani.simpledr.model;

public enum AsdrSignalPayloadEnum {

	NORMAL(0),
	MODERATE(1),
	HIGH(2),
	SPECIAL(3);
	
	/** Internal integer value associated to the item. */
	private int value;
	

	/**
	 * Constructor.
	 */
	private AsdrSignalPayloadEnum(int value) {
		this.value = value;
		return;
	}
	
	
	/**
	 * Return the integer value attached to the item of the enumeration.
	 */
	public int value() {
		return this.value;
	}

}
