package es.siani.simpledr.model;

import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


public class AsdrEvent  {
	
	/** Value for indicating that the event has not notification period. */
	public static final long NO_NOTIFICATION_PERIOD = 0;
	
	/** Value for indicating that the event has not priority. */
	public static final long NO_PRIORITY = 0;
	
	/** Default value for indicating whether the event requires a response. */
	public static final boolean DEFAULT_RESPONSE_REQUIRED = false;
	
	/** Priority of the event */
	protected long priority = NO_PRIORITY;	
	
	protected String eventId;
	protected AsdrSignalTypeEnum signalType;
	protected Date start;
	protected long duration;
	protected long notifDuration;
	protected boolean responseRequired = DEFAULT_RESPONSE_REQUIRED;
	protected AsdrTargets targets;
	protected List<AsdrInterval> intervals;
	
	
	
	/**
	 * Constructor.
	 */
	public AsdrEvent(AsdrSignalTypeEnum signalType, Date start, long duration, float payload) {
		this(signalType, start, NO_PRIORITY, NO_NOTIFICATION_PERIOD, Arrays.asList(new AsdrInterval(duration, payload)));
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public AsdrEvent(AsdrSignalTypeEnum signalType, Date start, long notifDuration, long priority, List<AsdrInterval> intervals) {
		
		if (intervals == null) {
			throw new IllegalArgumentException("At least one interval must be defined.");
		}
		
		// Check that the sum of the durations of all the intervals is equal
		// to the duration of the event.
		long totalDuration = 0;
		for (AsdrInterval interval : intervals) {
			totalDuration += interval.duration;
		}
		
		this.signalType = signalType;
		this.start = start;
		this.priority = priority;
		this.intervals = intervals;
		this.duration = totalDuration;
		this.notifDuration = notifDuration;
		this.targets = new AsdrTargets();
		
		return;
	}
	
	
	/**
	 * Get the EventId.
	 */
	public String getEventId() {
		return this.eventId;
	}
	
	
	/**
	 * Set the EventId.
	 */
	public void setEventId(String eventId) {
		this.eventId = eventId;
	}


	/**
	 * Get the SignalType.
	 */
	public AsdrSignalTypeEnum getSignalType() {
		return this.signalType;
	}


	/**
	 * Set the SignalType.
	 */
	public void setSignalType(AsdrSignalTypeEnum signalType) {
		this.signalType = signalType;
	}


	/**
	 * Get the Start.
	 */
	public Date getStart() {
		return this.start;
	}


	/**
	 * Set the Start.
	 */
	public void setStart(Date start) {
		this.start = start;
	}


	/**
	 * Get the Duration.
	 */
	public long getDuration() {
		return this.duration;
	}


	/**
	 * Set the Duration.
	 */
	public void setDuration(long duration) {
		this.duration = duration;
	}
	
	
	/**
	 * Get the NotificationDuration.
	 */
	public long getNotificationDuration() {
		return this.notifDuration;
	}
	
	
	/**
	 * Set the NotificationDuration.
	 */
	public void setNotificationDuration(long notificationDuration) {
		this.notifDuration = notificationDuration;
	}


	/**
	 * Get the Priority.
	 */
	public long getPriority() {
		return this.priority;
	}


	/**
	 * Set the Priority.
	 */
	public void setPriority(long priority) {
		this.priority = priority;
	}


	/**
	 * Get the ResponseRequired.
	 */
	public boolean isResponseRequired() {
		return this.responseRequired;
	}


	/**
	 * Set the ResponseRequired.
	 */
	public void setResponseRequired(boolean responseRequired) {
		this.responseRequired = responseRequired;
	}


	/**
	 * Get the Targets.
	 */
	public AsdrTargets getTargets() {
		return this.targets;
	}


	/**
	 * Set the Targets.
	 */
	public void setTargets(AsdrTargets targets) {
		this.targets = targets;
	}


	/**
	 * Get the Intervals.
	 */
	public List<AsdrInterval> getIntervals() {
		return this.intervals;
	}


	/**
	 * Set the Intervals.
	 */
	public void setIntervals(List<AsdrInterval> intervals) {
		this.intervals = intervals;
	}
	
	
	/**
	 * Build an array with the payloads of all the intervals.
	 */
	public float[] getIntervalsPayload() {
		if (this.intervals == null) {
			throw new IllegalStateException();
		}
		
		float[] payloads = new float[intervals.size()];
		Iterator<AsdrInterval> itIntervals = intervals.iterator();
		for (int n = 0; itIntervals.hasNext(); n++) {
			payloads[n] = itIntervals.next().payload;
		}
		
		return payloads;
	}

	
	/**
	 * Build an array with the duration of all the intervals.
	 */
	public long[] getIntervalsDuration() {
		if (this.intervals == null) {
			throw new IllegalStateException();
		}
		
		long[] durations = new long[intervals.size()];
		Iterator<AsdrInterval> itIntervals = intervals.iterator();
		for (int n = 0; itIntervals.hasNext(); n++) {
			durations[n] = itIntervals.next().duration;
		}
		
		return durations;		
	}

	
	/**
	 * Calculate the end time of the event.
	 */
	public Date getEndTime() {
		long endMillis = this.start.getTime();
		for (AsdrInterval interval : this.intervals) {
			endMillis += interval.getDuration() * 1000;
		}
		
		return new Date(endMillis);		
	}
}
