package es.siani.simpledr;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

public class OadrUtils {
	
	
	/**
	 * Constructor.
	 * This class is not instantiable.
	 */
	private OadrUtils() {
		return;
	}
	
	
	/**
	 * Return a XMLGregorianCalendar date from a classical date.
	 */
	static XMLGregorianCalendar toXMLGregorianCalendar(Date date) {
		
		GregorianCalendar gCalendar = new GregorianCalendar();
		gCalendar.setTime(date);
		XMLGregorianCalendar xmlCalendar = null;
		try {
			// Calling "normalize" function is important to produce XML dates in UTC format,
			// which is the supported by the OpenADR specification.
			xmlCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gCalendar).normalize();
		} catch (DatatypeConfigurationException ex) {
			getLogger().log(Level.SEVERE, null, ex);
		}
		
		return xmlCalendar;
	}
	
	
	/**
	 * Convert a XML duration string into milliseconds.
	 * It only supports a very simplified form of the XML duration format:
	 *    - PTxxxS
	 *    - PTxxxM
	 *    - PTxxxH
	 */
	static long xmlDurationToMilliseconds(String duration) {
		
		if (duration.length() < 4) {
			getLogger().log(Level.SEVERE, "The format of the Duration string is not supported.");
			throw new IllegalStateException();			
		} else if (!duration.startsWith("PT")) {
			getLogger().log(Level.SEVERE, "The format of the Duration string is not supported.");
			throw new IllegalStateException();
		} else if ((!duration.endsWith("S")) && (!duration.endsWith("M")) && (!duration.endsWith("H"))) {
			getLogger().log(Level.SEVERE, "The format of the Duration string is not supported.");
			throw new IllegalStateException();			
		}
		
		int number;
		try {
			number = Integer.parseInt(duration.substring(2, duration.length() - 1));
		} catch (NumberFormatException ex) {
			getLogger().log(Level.SEVERE, "Failed to parse the duration expression.");
			throw new IllegalStateException(ex);			
		}
		
		if (duration.endsWith("S")) {
			number *= 1000;
		} else if (duration.endsWith("M")) {
			number *= 1000 * 60;
		} else if (duration.endsWith("H")) {
			number *= 1000 * 3600;
		}
		
		return number;
	}
	
	
	/**
	 * Logger.
	 */
	private static final Logger getLogger() {
		return Logger.getLogger(OadrUtils.class.getName());
	}

}
