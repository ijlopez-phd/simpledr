package es.siani.simpledr;

import java.util.HashMap;
import java.util.Map;

public class OadrFactory {
	
	/** Cache of the ids of the last event created by each VTN. */
	protected static Map<String, Integer> lastEventsIds = new HashMap<String, Integer>();
	
	/** Cache of the ids of the last requests sent by each VTN */
	protected static Map<String, Integer> lastRequestsIds = new HashMap<String, Integer>();
	
	/** Cache of the ids of the last report created by each VTN. */
	protected static Map<String, Integer> lastReportsIds = new HashMap<String, Integer>();
	
	/** Code of the entity (VTN or VEN) that is using the factory */
	protected final String nodeCode;
	
	/** URI that identifies the market context */
	protected final String marketContext;
	
	
	/**
	 * Constructor.
	 */
	public OadrFactory(String nodeCode, String marketContext) {
		this.nodeCode = nodeCode;
		this.marketContext = marketContext;
		return;
	}
	
	
	/**
	 * Get the next event's id for the specified node.
	 */
	static String getNextEventId(String nodeCode) {
		
		Integer lastEventId = lastEventsIds.get(nodeCode);
		if (lastEventId == null) {
			lastEventId = -1;
		}
		
		lastEventsIds.put(nodeCode, ++lastEventId);
		
		return lastEventId.toString();
	}
	
	
	/**
	 * Get the last event's id associated to the specified node.
	 */
	static String getLasEventId(String nodeCode) {
		return lastEventsIds.get(nodeCode).toString();
	}
	
	
	/**
	 * Get the next report's id for the specified node.
	 */
	static String getNextReportId(String nodeCode) {
		
		Integer lastReportId = lastReportsIds.get(nodeCode);
		if (lastReportId == null) {
			lastReportId = -1;
		}
		
		lastReportsIds.put(nodeCode, ++lastReportId);
		
		return lastReportId.toString();
	}
	
	
	/**
	 * Get the last report's id associated to a specific node.
	 */
	static String getLastReportId(String nodeCode) {
		return lastReportsIds.get(nodeCode).toString();
	}
	
	
	/**
	 * Clear the cache of events' ids.
	 */
	static void resetEventsIds() {
		lastEventsIds.clear();
		return;
	}
	
	
	/**
	 * Get the next request's id for the specified node.
	 */
	static String getNextRequestId(String nodeCode) {
		
		Integer lastRequestId = lastRequestsIds.get(nodeCode);
		if (lastRequestId == null) {
			lastRequestId = -1;
		}
		
		lastRequestsIds.put(nodeCode, ++lastRequestId);
		
		return lastRequestId.toString();
	}
	
	
	/**
	 * Get the last request's id associated to the specified node.
	 */
	static String getLastRequestId(String nodeCode) {
		return lastRequestsIds.get(nodeCode).toString();
	}
	
	
	/**
	 * Clear the cache of requests' ids.
	 */
	static void resetRequestsIds() {
		lastRequestsIds.clear();
		return;
	}
	
	
	/**
	 * Clear the cache of reports' ids.
	 */
	static void resetReportsIds() {
		lastReportsIds.clear();
		return;
	}
	
	
//	/**
//	 * Logger.
//	 */
//    private static final Logger getLogger() {
//        return Logger.getLogger(OadrFactory.class.getName());
//    }	
}
