package es.siani.simpledr;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.transform.Source;

import es.siani.simpledr.model.openadr.OadrReport;
import es.siani.simpledr.model.openadr.OadrReportPayloadType;
import es.siani.simpledr.model.openadr.OadrUpdateReport;
import es.siani.simpledr.model.openadr.ObjectFactory;
import es.siani.simpledr.model.openadr.ei.Interval;
import es.siani.simpledr.model.openadr.ei.PayloadFloatType;
import es.siani.simpledr.model.openadr.ei.ReportPayloadType;
import es.siani.simpledr.model.openadr.strm.Intervals;
import es.siani.simpledr.model.openadr.strm.StreamPayloadBase;
import es.siani.simpledr.model.openadr.xcal.DateTime;
import es.siani.simpledr.model.openadr.xcal.DtStart;
import es.siani.simpledr.model.openadr.xcal.DurationPropType;
import es.siani.simpledr.model.openadr.xcal.DurationValue;
import es.siani.simpledr.xml.AsdrPrefixMapper;
import static es.siani.simpledr.OadrUtils.*;

/**
 * Create a message of the type OadrUpdateReport. It is sent in response to a OadrCreateReport.
 * This class is facade that simplifies the use of objects of the type OadrUpdateReport. The
 * characteristics of the message's content are:
 *    - It only contains ONE report.
 *    - ... and the report can only contain ONE interval.
 *
 */
public class EasyUpdateReport {
	
	protected final OadrUpdateReport obj;
	
	public EasyUpdateReport(String nodeCode, EasyCreateReport createReport, Map<String, Float> elems) {
		
		// Build the list of payloads that will contain the report.
		ObjectFactory of = new ObjectFactory();
		es.siani.simpledr.model.openadr.ei.ObjectFactory eiOf =
				new es.siani.simpledr.model.openadr.ei.ObjectFactory();
		
		List<JAXBElement<? extends StreamPayloadBase>> payloads =
				new ArrayList<JAXBElement<? extends StreamPayloadBase>>();
		for (Entry<String, Float> value : elems.entrySet()) {
			payloads.add(of.createOadrReportPayload(new OadrReportPayloadType()
				.withRID(value.getKey())
				.withPayloadBase(eiOf.createPayloadFloat(
						new PayloadFloatType().withValue(value.getValue())))));
		}
		
		
		XMLGregorianCalendar xmlStartTime = toXMLGregorianCalendar(createReport.getStartTime());
		String duration = "PT" + createReport.getDuration() + "S";
		this.obj = new OadrUpdateReport()
				.withRequestID(OadrFactory.getNextRequestId(nodeCode))
				.withOadrReports(new OadrReport()
					.withReportRequestID(createReport.getReportRequestId())
					.withEiReportID("0")
					.withReportSpecifierID(createReport.getSpecifierId())
					.withCreatedDateTime(
						new DateTime().withValue(
								toXMLGregorianCalendar(new Date())))
					.withDtstart(new DtStart().withDateTime(
						new DateTime().withValue(xmlStartTime)))
					.withDuration(new DurationPropType()
						.withDuration(new DurationValue().withValue(duration)))
					.withIntervals(
						new Intervals().withIntervals(
							new Interval()
								.withStreamPayloadBases(payloads)
								.withDtstart(new DtStart().withDateTime(new DateTime()
									.withValue(xmlStartTime)))
								.withDuration(new DurationPropType()
									.withDuration(new DurationValue().withValue(duration))))));
		
		return;
					
	}
	
	
	/**
	 * Constructor.
	 * Build a new EasyReport from a XML Source.
	 */
	public EasyUpdateReport(Source source) {
		
		try {
			Unmarshaller unmarshaller = JAXBContext.newInstance(OadrModel.NAMESPACE).createUnmarshaller();
			this.obj = (OadrUpdateReport)unmarshaller.unmarshal(source);
		} catch (JAXBException jex) {
			getLogger().log(Level.SEVERE, "Failed to unmarshall an OadrUpdateReport from the source.");
			throw new IllegalStateException(jex);
		}
		
		return;
	}	
	
	
	/**
	 * Get the OadrReport wrappered by the EasyReport.
	 */
	public OadrUpdateReport getReport() {
		return this.obj;
	}
	
	
	/**
	 * Return the XML that results from marshalling the object.
	 */
	@Override
	public String toString() {
		StringWriter ostream = new StringWriter();
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(OadrModel.NAMESPACE);
			Marshaller marshaller = jaxbContext.createMarshaller();
			marshaller.setProperty("com.sun.xml.bind.namespacePrefixMapper", new AsdrPrefixMapper());
			marshaller.marshal(this.obj, ostream);
		} catch (JAXBException ex) {
			getLogger().log(Level.SEVERE, "Failed to convert the OadrUpdateReport into a XML string.");
		}
		
		return ostream.toString();
	}
	
	
	/**
	 * Get the values of an interval as a Map. 
	 * For each entry of the map, the key means the rID and the value is the payload.
	 */	
	public Map<String, Float> getValues() {
		
		Interval interval;
		try {
			interval = this.obj.getOadrReports().get(0).getIntervals().getIntervals().get(0);
		} catch (Throwable t) {
			return null;
		}
		
		Map<String, Float> values = new HashMap<String, Float>();
		for (JAXBElement<? extends StreamPayloadBase> elem : interval.getStreamPayloadBases()) {
			@SuppressWarnings("unchecked")
			JAXBElement<ReportPayloadType> elemReport = (JAXBElement<ReportPayloadType>)elem;
			values.put(
					elemReport.getValue().getRID(),
					((PayloadFloatType)(elemReport.getValue().getPayloadBase().getValue())).getValue());
		}
		
		return values;
	}
	
	
	/**
	 * Logger.
	 */
	private static final Logger getLogger() {
		return Logger.getLogger(EasyUpdateReport.class.getName());
	}
}
