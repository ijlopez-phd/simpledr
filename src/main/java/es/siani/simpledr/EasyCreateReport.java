package es.siani.simpledr;

import static es.siani.simpledr.OadrUtils.toXMLGregorianCalendar;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.Source;

import es.siani.simpledr.model.openadr.OadrCreateReport;
import es.siani.simpledr.model.openadr.OadrReportRequest;
import es.siani.simpledr.model.openadr.ei.ReadingTypeEnumeratedType;
import es.siani.simpledr.model.openadr.ei.ReportSpecifier;
import es.siani.simpledr.model.openadr.ei.SpecifierPayload;
import es.siani.simpledr.model.openadr.xcal.DateTime;
import es.siani.simpledr.model.openadr.xcal.DtStart;
import es.siani.simpledr.model.openadr.xcal.DurationPropType;
import es.siani.simpledr.model.openadr.xcal.DurationValue;
import es.siani.simpledr.model.openadr.xcal.Properties;
import es.siani.simpledr.xml.AsdrPrefixMapper;
import static es.siani.simpledr.OadrUtils.*;

/**
 * Create a message of the type OadrCreateReport. This message is sent by a node (source)
 * to request reports to other node (target). The messages of the service, including this one,
 * may include a high number of options, thus resulting in complex messages. This class is aimed
 * to simplify the creation of this message by focusing in a common type of report. The characteristics
 * of the reports created by EasyCreateReport are:
 *   - The message only can request ONE report.
 *   - ... and the report request only can contain ONE interval.
 *   - The data is sent just after being collected.
 *   - 
 *
 */
public class EasyCreateReport {
	
	protected final OadrCreateReport obj;
	
	
	/**
	 * Constructor.
	 */
	public EasyCreateReport(String nodeCode, String specifierId, Date startTime, long duration, long granular, String[] rIds) {
		
		if ((nodeCode == null) || (specifierId == null) || (startTime == null) || (duration < 0) || (rIds == null)) {
			getLogger().log(Level.SEVERE, "The values passed to the constructor are not valid.");
			throw new IllegalArgumentException();
		}
		
		ArrayList<SpecifierPayload> sPayloads = new ArrayList<SpecifierPayload>();
		for (String rId : rIds) {
			sPayloads.add(new SpecifierPayload()
				.withRID(rId)
				.withReadingType(ReadingTypeEnumeratedType.ESTIMATED.value()));
		}
		
		this.obj = new OadrCreateReport()
			.withRequestID(OadrFactory.getNextRequestId(nodeCode))
			.withOadrReportRequests(new OadrReportRequest()
				.withReportRequestID(OadrFactory.getNextReportId(nodeCode))
				.withReportSpecifier(new ReportSpecifier()
						.withReportSpecifierID(specifierId)
						.withGranularity(new DurationPropType()
							.withDuration(new DurationValue().withValue("PT" + granular + "S")))
						.withReportBackDuration(new DurationPropType()
							.withDuration(new DurationValue().withValue("PT" + duration + "S")))
						.withReportInterval(new es.siani.simpledr.model.openadr.xcal.Interval()
							.withProperties(new Properties()
								.withDtstart(new DtStart().withDateTime(
										new DateTime().withValue(toXMLGregorianCalendar(startTime))))
								.withDuration(new DurationPropType().withDuration(
										new DurationValue().withValue("PT" + duration + "S")))))
						.withSpecifierPayloads(sPayloads)));
		
		return;
	}
	
	
	/**
	 * Constructor.
	 * Build a new EasyCreateReport from a XML source.
	 */
	public EasyCreateReport(Source source) {
		
		OadrCreateReport createReport;
		try {
			Unmarshaller unmarshaller = JAXBContext.newInstance(OadrModel.NAMESPACE).createUnmarshaller();
			createReport = (OadrCreateReport)unmarshaller.unmarshal(source);
		} catch (JAXBException jex) {
			getLogger().log(Level.SEVERE, "Failed to unmarshall an OadrCreateReport from the source.");
			throw new IllegalStateException(jex);			
		}
		
		if (createReport.getOadrReportRequests().size() != 1) {
			throw new IllegalArgumentException("The OadrCreateReport contained in the source is not supported by this class.");
		}
		
		Properties props = createReport.getOadrReportRequests().get(0).getReportSpecifier().getReportInterval().getProperties();
		String strDuration = props.getDuration().getDuration().getValue();
		if (! (strDuration.startsWith("PT") && ((strDuration.endsWith("M") || (strDuration.endsWith("S")))))) {
			throw new IllegalArgumentException("The format of the XML duration parameter is not supported.");
		}
		
		this.obj = createReport;
		
	}
	
	
	/**
	 * Get the request Id related to the CreateReport message.
	 */
	public String getRequestId() {
		return this.obj.getRequestID();
	}
	
	
	/**
	 * Get the Id related to the report request. By using EasyCreateReport only
	 * one Report can be requested at the same time.
	 */
	public String getReportRequestId() {
		return this.obj.getOadrReportRequests().get(0).getReportRequestID();
	}
	
	/**
	 * Get the Duration of the interval to which the data belongs.
	 */
	public long getDuration() {
		String strDuration = this.obj.getOadrReportRequests().get(0).getReportSpecifier()
				.getReportInterval().getProperties().getDuration().getDuration().getValue();
		
		return xmlDurationToMilliseconds(strDuration) / 1000;
	}
	
	
	/**
	 * Get the Granularity parameter in seconds.
	 */
	public long getGranularity() {
		String strGranularity =
				this.obj.getOadrReportRequests().get(0).getReportSpecifier().getGranularity().getDuration().getValue(); 
		return xmlDurationToMilliseconds(strGranularity) / 1000;
	}
	
	
	/**
	 * Get the SpecifierId of the requested report.
	 */
	public String getSpecifierId() {
		return this.obj.getOadrReportRequests().get(0).getReportSpecifier().getReportSpecifierID();
	}
	
	
	/**
	 * Get the StartTime of the interval to which the data belongs.
	 */
	public Date getStartTime() {
		return this.obj.getOadrReportRequests().get(0).getReportSpecifier()
				.getReportInterval().getProperties().getDtstart().getDateTime()
				.getValue().toGregorianCalendar().getTime();
	}
	
	
	/**
	 * Get the internal OadrCreateReport.
	 */
	public OadrCreateReport getCreateReport() {
		return this.obj;
	}
	
	
	/**
	 * Return the string that results from converting the object into XML.
	 */
	@Override
	public String toString() {
		StringWriter ostream = new StringWriter();
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(OadrModel.NAMESPACE);
			Marshaller marshaller = jaxbContext.createMarshaller();
			marshaller.setProperty("com.sun.xml.bind.namespacePrefixMapper", new AsdrPrefixMapper());
			marshaller.marshal(this.obj, ostream);
		} catch (JAXBException ex) {
			getLogger().log(Level.SEVERE, "Failed to convert the OadrReport into a XML string.");
		}
		
		return ostream.toString();
		
	}
	
	
	/**
	 * Logger.
	 */
	private static final Logger getLogger() {
		return Logger.getLogger(EasyCreateReport.class.getName());
	}
}
