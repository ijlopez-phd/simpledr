package es.siani.simpledr;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.xml.sax.SAXException;

import es.siani.simpledr.model.AsdrEvent;
import es.siani.simpledr.model.AsdrInterval;
import es.siani.simpledr.model.AsdrProgram;
import es.siani.simpledr.model.AsdrSignalTypeEnum;
import es.siani.simpledr.model.openadr.OadrDistributeEvent.OadrEvent;
import es.siani.simpledr.model.openadr.ei.EiEvent;
import es.siani.simpledr.model.openadr.ei.EiEventSignal;
import es.siani.simpledr.model.openadr.ei.Interval;
import es.siani.simpledr.model.openadr.ei.PayloadFloatType;
import es.siani.simpledr.model.openadr.ei.SignalPayload;
import es.siani.simpledr.xml.XMLAsdrProgramHandler;
import es.siani.simpledr.xml.XMLAsdrProgramHandler20aImpl;
import static es.siani.simpledr.OadrUtils.*;

public class AsdrUtils {
	
	static final String XSD_PATH = "/schemas/program/program_20a.xsd";

	
	/**
	 * Constructor.
	 * This class is not instantiable.
	 */
	private AsdrUtils() {
		return;
	}
	
	
	/**
	 * Create a DR program from a XML InputStream.
	 */
	public static AsdrProgram createProgram(InputStream xmlStream) throws AsdrException {
		
		// Validate the XML InputStream against the XSD Schema.
		XMLAsdrProgramHandler parserHandler = new XMLAsdrProgramHandler20aImpl();
		try {
			Schema schema = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema").newSchema(AsdrUtils.class.getResource(XSD_PATH));
			SAXParserFactory saxFactory = SAXParserFactory.newInstance();
			saxFactory.setNamespaceAware(true);
			//saxFactory.setValidating(true); // Generates this error message: "Document is invalid: no grammar found".
			saxFactory.setSchema(schema);			
			saxFactory.newSAXParser().parse(xmlStream, parserHandler);
		} catch (ParserConfigurationException e) {
			throw new AsdrException("The SAXParser factory could not be created.", e);
		} catch (SAXException e) {
			throw new AsdrException("SAX errors occurred during processing.", e);
		} catch (IOException e) {
			throw new AsdrException("SAX errors occurred during processing.", e);
		}
		
		return parserHandler.getProgram();
	}
	
	@SuppressWarnings("unchecked")
	static AsdrEvent toAsdrEvent(OadrEvent oadrEvent) {
		
		final EiEvent eiEvent = oadrEvent.getEiEvent();
		
		List<AsdrInterval> asdrIntervals = new ArrayList<AsdrInterval>();
		for (Interval oadrInterval : eiEvent.getEiEventSignals().getEiEventSignals().get(0).getIntervals().getIntervals()) {
			asdrIntervals.add(new AsdrInterval(
					xmlDurationToMilliseconds(oadrInterval.getDuration().getDuration().getValue()) / 1000,
					((JAXBElement<PayloadFloatType>)
								((JAXBElement<SignalPayload>)oadrInterval.getStreamPayloadBases().get(0))
								.getValue().getPayloadBase()).getValue().getValue()));
		}
		
		final EiEventSignal eiSignal = eiEvent.getEiEventSignals().getEiEventSignals().get(0); 
		AsdrEvent asdrEvent = new AsdrEvent(
				AsdrSignalTypeEnum.valueOf(eiSignal.getSignalType().value().toUpperCase()),
				eiEvent.getEiActivePeriod().getProperties().getDtstart().getDateTime().getValue().toGregorianCalendar().getTime(),
				xmlDurationToMilliseconds(
						eiEvent.getEiActivePeriod().getProperties().getXEiNotification().getDuration().getValue()) / 1000,
				eiEvent.getEventDescriptor().getPriority(),
				asdrIntervals);
		asdrEvent.setEventId(eiEvent.getEventDescriptor().getEventID());
		return asdrEvent;
	}
	
	
//	/**
//	 * Logger.
//	 */
//	private static final Logger getLogger() {
//		return Logger.getLogger(AsdrUtils.class.getName());
//	}
}
