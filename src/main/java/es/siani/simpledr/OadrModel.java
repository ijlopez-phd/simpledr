package es.siani.simpledr;

public final class OadrModel {
	
	public static final String NAMESPACE = "es.siani.simpledr.model.openadr"
			+ ":es.siani.simpledr.model.openadr.atom"
			+ ":es.siani.simpledr.model.openadr.currency"
			+ ":es.siani.simpledr.model.openadr.ei"
			+ ":es.siani.simpledr.model.openadr.emix"
			+ ":es.siani.simpledr.model.openadr.gml"
			+ ":es.siani.simpledr.model.openadr.greenbutton"
			+ ":es.siani.simpledr.model.openadr.power"
			+ ":es.siani.simpledr.model.openadr.pyld"
			+ ":es.siani.simpledr.model.openadr.siscale"
			+ ":es.siani.simpledr.model.openadr.strm"
			+ ":es.siani.simpledr.model.openadr.xcal"
			+ ":es.siani.simpledr.model.openadr.xmldisg11"
			+ ":es.siani.simpledr.model.openadr.xmldsig";

}
