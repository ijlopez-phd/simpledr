package es.siani.simpledr;

@SuppressWarnings("serial")
public class AsdrException extends Exception{
	
	/**
	 * Constructor.
	 */
	public AsdrException() {
		super();
	}
	
	
	/**
	 * Constructor.
	 */
	public AsdrException(String message, Throwable cause) {
		super(message, cause);
	}
	
	
	/**
	 * Constructor.
	 */
	public AsdrException(Throwable cause) {
		super(cause);
	}

}
