package es.siani.simpledr;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.Source;

import es.siani.simpledr.model.AsdrEvent;
import es.siani.simpledr.model.AsdrInterval;
import es.siani.simpledr.model.AsdrSignalPayloadEnum;
import es.siani.simpledr.model.openadr.OadrDistributeEvent;
import es.siani.simpledr.model.openadr.ResponseRequiredType;
import es.siani.simpledr.xml.AsdrPrefixMapper;
import es.siani.simpledr.model.openadr.OadrDistributeEvent.OadrEvent;
import es.siani.simpledr.model.openadr.ei.CurrentValue;
import es.siani.simpledr.model.openadr.ei.EiActivePeriod;
import es.siani.simpledr.model.openadr.ei.EiEvent;
import es.siani.simpledr.model.openadr.ei.EiEventSignal;
import es.siani.simpledr.model.openadr.ei.EiEventSignals;
import es.siani.simpledr.model.openadr.ei.EiTarget;
import es.siani.simpledr.model.openadr.ei.EventDescriptor;
import es.siani.simpledr.model.openadr.ei.EventDescriptor.EiMarketContext;
import es.siani.simpledr.model.openadr.ei.EventStatusEnumeratedType;
import es.siani.simpledr.model.openadr.ei.ObjectFactory;
import es.siani.simpledr.model.openadr.ei.PayloadFloatType;
import es.siani.simpledr.model.openadr.ei.SignalPayload;
import es.siani.simpledr.model.openadr.ei.SignalTypeEnumeratedType;
import es.siani.simpledr.model.openadr.ei.Interval;
import es.siani.simpledr.model.openadr.emix.MarketContext;
import es.siani.simpledr.model.openadr.strm.Intervals;
import es.siani.simpledr.model.openadr.xcal.DateTime;
import es.siani.simpledr.model.openadr.xcal.DtStart;
import es.siani.simpledr.model.openadr.xcal.DurationPropType;
import es.siani.simpledr.model.openadr.xcal.DurationValue;
import es.siani.simpledr.model.openadr.xcal.Properties;
import es.siani.simpledr.model.openadr.xcal.Uid;
import static es.siani.simpledr.OadrUtils.*;

public class EasyDistributeEvent {
	
	protected final String marketContext;
	protected final String nodeCode;
	protected final OadrDistributeEvent obj;

	
	/**
	 * Constructor.
	 */
	public EasyDistributeEvent(String marketContext, String nodeCode) {
		this.marketContext = marketContext;
		this.nodeCode = nodeCode;
		
		this.obj = new OadrDistributeEvent();
		obj.setVtnID(nodeCode);
		obj.setRequestID(OadrFactory.getNextRequestId(nodeCode));
		
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public EasyDistributeEvent(String marketContext, OadrDistributeEvent other) {
		this.obj = other;
		this.nodeCode = other.getVtnID();
		this.marketContext = marketContext;
		return;
	}
	
	
	/**
	 * Constructor.
	 */
	public EasyDistributeEvent(String marketContext, Source source) {
		
		OadrDistributeEvent distributeEvent;
		try {
			Unmarshaller unmarshaller = JAXBContext.newInstance(OadrModel.NAMESPACE).createUnmarshaller();
			distributeEvent = (OadrDistributeEvent)unmarshaller.unmarshal(source);
		} catch (JAXBException jex) {
			getLogger().log(Level.SEVERE, "Failed to create the JAXB Context.", jex);
			throw new IllegalStateException(jex);
		}
		
		this.marketContext = marketContext;
		this.nodeCode = distributeEvent.getVtnID();
		this.obj = distributeEvent;
		
		return;
	}
	
	
	/**
	 * Get the market context.
	 */
	public String getMarketContext() {
		return this.marketContext;
	}
	
	
	/**
	 * Gets the OadrDistributeEvent wrappered by this instance. 
	 */
	public OadrDistributeEvent getDistributeEvent() {
		return this.obj;
	}
	
	
	/**
	 * Returns the XML that results from marshalling the object.
	 */
	@Override
	public String toString() {
		StringWriter ostream = new StringWriter();
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(OadrModel.NAMESPACE);
			Marshaller marshaller = jaxbContext.createMarshaller();
			marshaller.setProperty("com.sun.xml.bind.namespacePrefixMapper", new AsdrPrefixMapper());
			marshaller.marshal(this.obj, ostream);
		} catch (JAXBException ex) {
			getLogger().log(Level.SEVERE, "Failed to convert the OadrDistributeEvent to a XML string.");
		}
		
		return ostream.toString();
	}
	
	
	/**
	 * Remove an OadrEvent from the message.
	 */
	public void removeEvent(String eventId) {
		
		Iterator<OadrEvent> it = obj.getOadrEvents().iterator();
		while (it.hasNext()) {
			if (it.next().getEiEvent().getEventDescriptor().getEventID().equals(eventId)) {
				it.remove();
				break;
			}
		}
		
		return;
	}	
	
	
	/**
	 * Remove the events that have been cancelled or that are
	 * finished.
	 */
	public void removeFinishedEvents() {
		
		// Gather all the events which status is cancelled or completed.
		ArrayList<OadrEvent> toBeRemoved = new ArrayList<OadrEvent>();
		for (OadrEvent oadrEvent : obj.getOadrEvents()) {
			final EventStatusEnumeratedType status = oadrEvent.getEiEvent().getEventDescriptor().getEventStatus(); 
			if ((status == EventStatusEnumeratedType.CANCELLED) || (status == EventStatusEnumeratedType.COMPLETED)) {
				toBeRemoved.add(oadrEvent);
			}
		}
		
		obj.getOadrEvents().removeAll(toBeRemoved);
		
		return;
	}
	
	
	/**
	 * Update the state of the existent events according to a date.
	 * Events' state may change from FAR to NEAR or ACTIVE.
	 */
	public void updateEventsStatus(long currentTime) {
		
		for (OadrEvent oadrEvent : obj.getOadrEvents()) {
			
			if (oadrEvent.getEiEvent().getEventDescriptor().getEventStatus() ==
					EventStatusEnumeratedType.CANCELLED) {
				// Previously cancelled events are not updated.
				continue;
			}
			
			EventStatusEnumeratedType status = EventStatusEnumeratedType.NONE;
			EiActivePeriod ap = oadrEvent.getEiEvent().getEiActivePeriod();
			final long start = 
					ap.getProperties().getDtstart().getDateTime().getValue().toGregorianCalendar().getTimeInMillis();
			final long duration = 
					xmlDurationToMilliseconds(ap.getProperties().getDuration().getDuration().getValue());
			final long notif = 
					xmlDurationToMilliseconds(ap.getProperties().getXEiNotification().getDuration().getValue());
			
			if ((currentTime < (start)) && (currentTime >= (start - notif))) {
				status = EventStatusEnumeratedType.FAR;
				
			} else if ((currentTime >= start) && (currentTime < (start + duration))) {
				status = EventStatusEnumeratedType.ACTIVE;
				
			} else if (currentTime >= (start + duration)) {
				status = EventStatusEnumeratedType.COMPLETED;
			}
			
			oadrEvent.getEiEvent().getEventDescriptor().setEventStatus(status);
		}
		
		return;
	}

	
	/**
	 * Determines the status that corresponds to an AsdrEvent.
	 */
	static EventStatusEnumeratedType getEventStatus(AsdrEvent event, final long currentTime) {
		EventStatusEnumeratedType status;
		
		final long start = event.getStart().getTime();
		final long notifDuration = event.getNotificationDuration() * 1000;
		final long end = start + event.getDuration() * 1000;
		
		if (currentTime < (start - notifDuration)) {
			status = EventStatusEnumeratedType.NONE;
		} else if (currentTime < start) {
			status = EventStatusEnumeratedType.FAR;
		} else if (currentTime < end) {
			status = EventStatusEnumeratedType.ACTIVE;
		} else {
			status = EventStatusEnumeratedType.COMPLETED;
		}
		
		return status;
	}
	
	
	/**
	 * Add a new OadrEvent to the message.
	 */
	@SuppressWarnings("unchecked")
	public String addEvent(AsdrEvent event, final long currentTime) {
		
		// Build the structure of intervals.
		List<Interval> intervals = new ArrayList<Interval>();
		int nInterval = 0;
		ObjectFactory of = new ObjectFactory();
		for (AsdrInterval asdrInterval : event.getIntervals()) {
			intervals.add(new Interval()
					.withDuration(new DurationPropType().withDuration(
							new DurationValue().withValue("PT" + asdrInterval.getDuration() + "S")))
					.withUid(new Uid().withText(Integer.toString(nInterval++)))
					.withStreamPayloadBases(
							of.createSignalPayload(new SignalPayload().withPayloadBase(
									of.createPayloadFloat(new PayloadFloatType().withValue(asdrInterval.getPayload()))))));
		}
		
		// Build the event and add it to the OadrDistributeEvent message.
		final String eventId = 
				(event.getEventId() == null)? OadrFactory.getNextEventId(this.nodeCode) : event.getEventId();
		obj.withOadrEvents(new OadrEvent()
			.withOadrResponseRequired(
					event.isResponseRequired()? ResponseRequiredType.ALWAYS : ResponseRequiredType.NEVER)
			.withEiEvent(new EiEvent()
				.withEventDescriptor(
						new EventDescriptor()
							.withEventID(eventId)
							.withModificationNumber(0)
							.withPriority(event.getPriority())
							.withCreatedDateTime(new DateTime().withValue(toXMLGregorianCalendar(new Date())))
							.withEventStatus(getEventStatus(event, currentTime))
							.withEiMarketContext( 
									new EiMarketContext().withMarketContext(
											new MarketContext().withValue(this.marketContext))))
				.withEiActivePeriod(
						new EiActivePeriod().withProperties(new Properties()
							.withDtstart(
									new DtStart().withDateTime(new DateTime().withValue(toXMLGregorianCalendar(event.getStart()))))
							.withDuration(new DurationPropType().withDuration(
									new DurationValue().withValue("PT" + event.getDuration() + "S")))
							.withXEiNotification(new DurationPropType().withDuration(
									new DurationValue().withValue("PT" + event.getNotificationDuration() + "S")))))
				.withEiTarget(
						new EiTarget()
							.withGroupIDs(event.getTargets().groups)
							.withPartyIDs(event.getTargets().parties)
							.withResourceIDs(event.getTargets().resources)
							.withVenIDs(event.getTargets().vendors))
				.withEiEventSignals(
						new EiEventSignals()
							.withEiEventSignals(
									new EiEventSignal()
										.withSignalName("simple")
										.withSignalType(SignalTypeEnumeratedType.fromValue(event.getSignalType().value()))
										.withSignalID("0")
										.withIntervals(
												new Intervals().withIntervals(intervals))
										.withCurrentValue(new CurrentValue().withPayloadFloat(
												new PayloadFloatType().withValue(AsdrSignalPayloadEnum.NORMAL.value())))))));
		return eventId;
	}
	
	
	/**
	 * Look for events registered in other instance of EasyDistributeEvent that are not
	 * registered in this instance.
	 */
	public List<AsdrEvent> findNewEvents(EasyDistributeEvent otherEasy) {
		
		if (otherEasy == null)  {
			throw new IllegalArgumentException();
		}		
		
		List<AsdrEvent> newEvents = new ArrayList<AsdrEvent>();
		
		List<OadrEvent> thisEvents = this.obj.getOadrEvents();
		List<OadrEvent> otherEvents = otherEasy.getDistributeEvent().getOadrEvents();
		for (OadrEvent  otherEvent : otherEvents) {
			final String otherId = otherEvent.getEiEvent().getEventDescriptor().getEventID();
			boolean found = false;
			for (OadrEvent thisEvent : thisEvents) {
				if (thisEvent.getEiEvent().getEventDescriptor().getEventID().equals(otherId)) {
					found = true;
					break;
				}
			}
			
			if (!found) {
				newEvents.add(AsdrUtils.toAsdrEvent(otherEvent));
			}
		}
		
		return newEvents;
	}
	
	
	/**
	 * Logger.
	 */
	private static final Logger getLogger() {
		return Logger.getLogger(EasyDistributeEvent.class.getName());
	}
}
