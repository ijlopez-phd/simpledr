package es.siani.simpledr.xml;

import org.xml.sax.helpers.DefaultHandler;

import es.siani.simpledr.model.AsdrProgram;

public abstract class XMLAsdrProgramHandler extends DefaultHandler {
	
	/**
	 * Get the DRProgram object corresponding to the XML content.
	 */
	public abstract AsdrProgram getProgram();

}
