package es.siani.simpledr.xml;

import com.sun.xml.bind.marshaller.NamespacePrefixMapper;



public class AsdrPrefixMapper extends NamespacePrefixMapper {
	
	public static final String URI_OADR = "http://openadr.org/oadr-2.0b/2012/07";
	public static final String URI_EI = "http://docs.oasis-open.org/ns/energyinterop/201110";
	public static final String URI_PYLD = "http://docs.oasis-open.org/ns/energyinterop/201110/payloads";
	public static final String URI_XCAL = "urn:ietf:params:xml:ns:icalendar-2.0";
	public static final String URI_STRM = "urn:ietf:params:xml:ns:icalendar-2.0:stream";
	public static final String URI_EMIX = "http://docs.oasis-open.org/ns/emix/2011/06";
	public static final String URI_DS = "http://www.w3.org/2000/09/xmldsig#";
	public static final String URI_DSIG11 = "http://www.w3.org/2009/xmldsig11#";
	public static final String URI_CURRENCY = "urn:un:unece:uncefact:codelist:standard:5:ISO42173A:2010-04-07";
	public static final String URI_SCALE = "http://docs.oasis-open.org/ns/emix/2011/06/siscale";
	public static final String URI_POWER = "http://docs.oasis-open.org/ns/emix/2011/06/power";
	public static final String URI_GB = "http://naesb.org/espi";
	public static final String URI_ATOM = "http://www.w3.org/2005/Atom";
	
	
	

	@Override
	public String getPreferredPrefix(String namespaceUri, String suggestion, boolean required) {
		
		if (namespaceUri.equals(URI_EI)) {
			return "ei";
		} else if (namespaceUri.equals(URI_PYLD)) {
			return "pyld";
		} else if (namespaceUri.equals(URI_XCAL)) {
			return "xcal";
		} else if (namespaceUri.equals(URI_STRM)) {
			return "strm";
		} else if (namespaceUri.equals(URI_EMIX)) {
			return "emix";
		} else if (namespaceUri.equals(URI_OADR)) {
			return "oadr";
		} else if (namespaceUri.equals(URI_DS)) {
			return "ds";
		} else if (namespaceUri.equals(URI_DSIG11)) {
			return "dsig11";
		} else if (namespaceUri.equals(URI_CURRENCY)) {
			return "clm5ISO42173A";
		} else if (namespaceUri.equals(URI_SCALE)) {
			return "scale";
		} else if (namespaceUri.equals(URI_POWER)) {
			return "power";
		} else if (namespaceUri.equals(URI_GB)) {
			return "gb";
		} else if (namespaceUri.equals(URI_ATOM)) {
			return "atom";
		}
		
		return suggestion;
	}

}
