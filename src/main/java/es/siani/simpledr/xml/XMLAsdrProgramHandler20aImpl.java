package es.siani.simpledr.xml;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import es.siani.simpledr.model.AsdrEvent;
import es.siani.simpledr.model.AsdrInterval;
import es.siani.simpledr.model.AsdrProgram;
import es.siani.simpledr.model.AsdrSignalTypeEnum;



public class XMLAsdrProgramHandler20aImpl extends XMLAsdrProgramHandler {
	
	private AsdrProgram program;
	private AsdrSignalTypeEnum signalType;
	private Date signalStart;
	private List<AsdrInterval> intervals;
	private long priority;
	private long notifDuration;
	
	
	/**
	 * Constructor.
	 */
	public XMLAsdrProgramHandler20aImpl() {
		return;
	}
	

	/**
	 * @see XMLAsdrProgramHandler#getProgram()
	 */
	@Override
	public AsdrProgram getProgram() {
		return this.program;
	}
	
	
	/**
	 * @see org.xml.sax.helpers.DefaultHandler#startElement(String, String, String, Attributes)
	 */
	@Override
	public void startElement(String uri, String localName, String qName, Attributes attr) throws SAXException {
		
		if (localName.equals("program")) {
			this.program = new AsdrProgram(attr.getValue("name"));
			
		} else if (localName.equals("event")) {
			try {
				this.signalStart = 
						DatatypeFactory.newInstance().
							newXMLGregorianCalendar(attr.getValue("start")).normalize().toGregorianCalendar().getTime();
			} catch (DatatypeConfigurationException e) {
				throw new SAXException(e);
			}
			
			this.signalType = AsdrSignalTypeEnum.valueOf(attr.getValue("type").toUpperCase());
			this.intervals = new ArrayList<AsdrInterval>();
			this.priority =
					(attr.getValue("priority") == null)? 0 : Long.parseLong(attr.getValue("priority"));
			this.notifDuration = 
					(attr.getValue("notifDuration") == null)? 0 : Long.parseLong(attr.getValue("notifDuration"));
			
		} else if (localName.equals("interval")) {
			this.intervals.add(new AsdrInterval(
					Long.parseLong(attr.getValue("duration")),
					Float.parseFloat(attr.getValue("value"))));
		}
		
		return;
	}
	
	
	/**
	 * @see org.xml.sax.helpers.DefaultHandler#endElement(String, String, String)
	 */	
	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		if (localName.equals("event")) {
			this.program.addEvent(
					new AsdrEvent(this.signalType, this.signalStart, this.notifDuration, this.priority, this.intervals));
		}
	}
	
	
	/**
	 * @see org.xml.sax.helpers.DefaultHandler#error(SAXParseException)
	 */
	@Override
    public void error (SAXParseException e) throws SAXException {
		throw e;
	}
	
	
	/**
	 * @see org.xml.sax.helpers.DefaultHandler#fatalError(SAXParseException)
	 */
	@Override
	public void fatalError(SAXParseException e) throws SAXException {
		throw e;
	}
	
	
	/**
	 * @see org.xml.sax.helpers.DefaultHandler#warning(SAXParseException)
	 */
	@Override
	public void warning(SAXParseException e) throws SAXException {
		getLogger().log(Level.WARNING, "Validating XML program against XSD.", e);
	}
	
	
	/**
	 * Logger.
	 */
	private static final Logger getLogger() {
		return Logger.getLogger(XMLAsdrProgramHandler20aImpl.class.getName());
	}	
}
