
## Generating the OpenADR classes from the XSD schemas ##

Type the following command in this folder:

    xjc -npa -mark-generated -extension -d ./../../java/ ./../profiles/2.0a/schemas/ -b ./oadr20a.xjb.xml


    